#### Agenda:
     -Помнишь эту фотку?
     -Даа, мы ж тогда были так голодны, что заточили по два здоровенных сэндвича и те шоколадыне шарики
     -Ага, не помнишь, как они называются?
 

![task.jpg](https://gitlab.com/UralCTF/uralctf-2019-writeups/raw/master/Do%20you%20like%20travelling%3F/images/5eb7e0f5.jpg)
 
 --- 
Одной из примечательных вещей на фотографии является обрезанная таблица с улицей  


Разобравшись с написанием ø, забиваем известную нам часть улицы в Gmaps и получаем:


![54b17185.png](https://gitlab.com/UralCTF/uralctf-2019-writeups/raw/master/Do%20you%20like%20travelling%3F/images/54b17185.png)

Фьерд **Høgsfjorden**
И нескололько улиц **Høgsfjordgata**
Поискав церкви, находим нашу:
![ca76b25d.png](https://gitlab.com/UralCTF/uralctf-2019-writeups/raw/master/Do%20you%20like%20travelling%3F/images/ca76b25d.png)

Итак, знакомьтесь, **St. Johannes** в провинции *Scavenger*!

Зная локацию, можем искать и где путешественники могут перекусть сэндвичами вблизи
![de6663c0.png](https://gitlab.com/UralCTF/uralctf-2019-writeups/raw/master/Do%20you%20like%20travelling%3F/images/de6663c0.png)

Первое заведение из списка как раз находится неподалеку

![108b7419.png](https://gitlab.com/UralCTF/uralctf-2019-writeups/raw/master/Do%20you%20like%20travelling%3F/images/108b7419.png)


На сайте есть витрина, на которой можем найти что-то шоколадное и круглое, подхлдящее под описание:
![ed298e4d.png](https://gitlab.com/UralCTF/uralctf-2019-writeups/raw/master/Do%20you%20like%20travelling%3F/images/ed298e4d.png)

**Flag:** UralCTF{Daddelbombe}
