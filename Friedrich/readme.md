**Описание:**
> Four parts of Tequila, one part of Cointreau and one part of lime juice. Save it as jpg, please

Также дан файл с содержанием с содержимым в 16-ом виде на очень много строк
```
ffffffff8080ffffffff8080ffffffff8080ffffffff8080ffffffff8080ff...
```
Из описания возьмем ключевое: 4 к 1 к 1 (4:1:1) и jpg
Первая же ссылка по запросу ведет на wiki - статью о [Chroma subsampling](https://en.wikipedia.org/wiki/Chroma_subsampling). Из которой можно узнать что строки нужны интерпритировать как блоки по 8 символов, кодирующие 4 пикселя:
 - 4 значения как их яркости
- 5-ое как относительная голубизна группы из 4 пикселей
 - 6-ое как относительная зеленость той же группы

Привести изображение к формату к RGB можно разными способами.  например развернув копию этого [гайда](https://github.com/ParametricPress/01-unraveling-the-jpeg).
Получив изображение оказывается, что это еще не конец и флаг не то, чтобы читаем:

![d0b65517.png](./images/d0b65517.png)

На этом этапе видя размытие и обративши внимание на название задания, следует заняться поисками способа избавиться от размытия Фридерика Гаусса.

Тут очень помогает отечеcтвенный проект [SmartDeblur](http://smartdeblur.net/).

![3635d238.png](./images/3635d238.png)

Резкость изображения повысилась достаточно, чтобы прочитать флаг.

**Flag:** UralCTF{gauss_and_hack}
